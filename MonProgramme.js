console.log("coucou");

let donneesDEntrainement = [
    {taille: 30, poids:500},
    {taille: 25, poids:450},
    {taille: 35, poids:550},
	{taille: 15, poids:200},
    {taille: 3, poids:5},
    {taille: 5, poids:6},
    {taille: 2, poids:4}
]

let reponses = [1, 1, 1, 0.5, 0, 0, 0];

reseau = new Reseau();

reseau.entrainerSur(donneesDEntrainement, reponses);

console.log("Chances que ça soit un concombre : " + reseau.predict(30, 500).toFixed(10));
console.log("Chances que ça soit un concombre (demi) : " + reseau.predict(14, 190).toFixed(10));
console.log("Chances que ça soit un concombre (cornichon) : " + reseau.predict(3, 5).toFixed(10));