class Neurone {
    constructor() {
        this.poidsBranche1 = getRandomBetweenMinus1and1();
        this.poidsBranche2 = getRandomBetweenMinus1and1();
        this.biais = getRandomBetweenMinus1and1();

        this.vieuxPoidsBranche1 = 0;
        this.vieuxPoidsBranche2 = 0;
        this.vieuxBiais = 0;
    }

    calcule(entreeBranche1, entreeBranche2) {
        let calc = (this.poidsBranche1 * entreeBranche1) + 
        (this.poidsBranche2 * entreeBranche2)
        + this.biais;
        return sigmoid(calc);
    }

    muter() {
        let proprieteAChanger = getRandomIntBetween(1, 3);
        let puissanceChangement = getRandomBetweenMinus1and1();
        if (proprieteAChanger == 1) {
            this.poidsBranche1 += puissanceChangement;
        } else if (proprieteAChanger == 2) {
            this.poidsBranche2 += puissanceChangement;
        } else {
            this.biais += puissanceChangement;
        }
    }

    oublierMutation() {
        this.poidsBranche1 = this.vieuxPoidsBranche1;
        this.poidsBranche2 = this.vieuxPoidsBranche2;
        this.biais = this.vieuxBiais;
    }

    retenirMutation() {
        this.vieuxPoidsBranche1 = this.poidsBranche1;
        this.vieuxPoidsBranche2 = this.poidsBranche2;
        this.vieuxBiais = this.biais;
    }
}