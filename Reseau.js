class Reseau {
    constructor() {
        this.neurones = [
            new Neurone(), new Neurone(), new Neurone(), //couche d'entrée
            new Neurone(), new Neurone(), //couche cachée
            new Neurone() //couche de sortie
        ];
    }

    predict(entree1, entree2) {
        return this.neurones[5].calcule(
            this.neurones[3].calcule(
                this.neurones[1].calcule(entree1, entree2),
                this.neurones[2].calcule(entree1, entree2)
            ),
            this.neurones[4].calcule(
                this.neurones[0].calcule(entree1, entree2),
                this.neurones[1].calcule(entree1, entree2)
            )
        );
    }

    entrainerSur(donneesDEntrainement, reponses) {
        let plusBasTauxDErreur = 9999;
        for(let epoque = 0; epoque < 1000; epoque++) {
            let r = getRandomIntBetween(0, 6);
            let neuroneMutant = this.neurones[r];
            neuroneMutant.muter();
            let predictions = [];
            for(let donneeDEntrainement of donneesDEntrainement) {
                predictions.push(this.predict(donneeDEntrainement.taille, donneeDEntrainement.poids));                
            }
            // déterminer si on est proche des solutions !!!
            let tauxDErreurDeCetteEpoque = meanSquareLoss(reponses, predictions);
            
            if (tauxDErreurDeCetteEpoque < plusBasTauxDErreur) {
                plusBasTauxDErreur = tauxDErreurDeCetteEpoque;
                neuroneMutant.retenirMutation();
            } else {
                neuroneMutant.oublierMutation();
            }
            console.log("Epoque " + epoque + ", erreur de la meilleur epoque = " + plusBasTauxDErreur + ", erreurs de cette epoque = " + tauxDErreurDeCetteEpoque);
            if (plusBasTauxDErreur < 0.001) {
                break;
            }
        }
    }
}