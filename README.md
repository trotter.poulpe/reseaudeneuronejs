Réseau de neurone simple en Javascript. Pour simplifier, un neurone aura TOUJOURS 2 entrées.

Tutoriel : https://youtu.be/EWCo2UdOxKc

Code : http://traitplat.fr/rdn/

![Structure du réseau](reseau.png)

Basé sur ce tutoriel : https://www.infoworld.com/article/3685569/how-to-build-a-neural-network-in-java.html
